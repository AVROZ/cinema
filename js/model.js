
let model = {
    key : "402d02d7e7ea966a38a59a715b16ced7",
    genres: {},
    get: (url, args) => {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.onload = function() {
                if(xhr.status != 200){
                    reject(this.response);
                }
                resolve(JSON.parse(this.response));
            }
            xhr.onerror = function(){
                reject(this);
            }
            url += "?api_key="+model.key;
            for( arg in args){
                url += "&"+arg+"="+args[arg];
            }
            xhr.open("GET",url);
            xhr.send();
        });
    },
    updateGenres: () => {
        model.get("https://api.themoviedb.org/3/genre/movie/list", {}).then((rep) => {
            for( genre of rep.genres){
                model.genres[genre.id]=genre.name;
            }
        });
    }
}